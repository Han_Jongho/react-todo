import React from 'react';

type TodoCounterProps = {
    count: number;
}

function TodoCounter({count}: TodoCounterProps) {
    const msg: string = 
        count === 0 ? 'Everything is Done!!' : `${count} To-dos left`;

    return (
        <h1>{msg}</h1>
    )
}

export default TodoCounter;